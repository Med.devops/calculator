package com.leszko.calculator;

import org.springframework.stereotype.Service;

@Service
class Calculator {
    int sum(int a, int b) {
        return a + b;
    }
    int substract(int a, int b ){
        return a - b;
    }
    int mult(int a, int b) {
        return a * b;
    }
    int div(int a, int b ){
        return a / b;
    }
}
