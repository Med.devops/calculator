package com.leszko.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
    @Autowired
    private Calculator calculator;

    @RequestMapping("/sum")
    public String sum(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        String resultat = "";
        resultat = a + "+" + b + "=" + String.valueOf(calculator.sum(a, b));
        return resultat;
    }
    @RequestMapping("/substract")
    public String substract(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        String resultat = "";
        resultat = a + "-" + b + "=" + String.valueOf(calculator.substract(a, b));
        return resultat;
    }
    @RequestMapping("/mult")
    public String mult(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        String resultat = "";
        resultat = a + "*" + b + "=" + String.valueOf(calculator.mult(a, b));
        return resultat;
    }
    @RequestMapping("/div")
    public String div(@RequestParam("a") Integer a, @RequestParam("b") Integer b) {
        String resultat = "";
        resultat = a + "/" + b + "=" + String.valueOf(calculator.div(a, b));
        return resultat;
    }
}
